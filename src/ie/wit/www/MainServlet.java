package ie.wit.www;

import java.io.IOException;
import java.util.Random;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class MainServlet
 */
@WebServlet("/MainServlet")
public class MainServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MainServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		Beer beer = getRandBeer();
		request.setAttribute("name", beer.getName());
		request.setAttribute("desc", beer.getDesc());
		request.setAttribute("ABV", beer.getPerc());
		request.setAttribute("location", beer.getLoc());
		request.getRequestDispatcher("/index.jsp").forward(request, response);

		// response.getWriter().append("Served at:
		// ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

	private Beer getRandBeer() {

		Beer[] beer = new Beer[10];
		beer[0] = new Beer("Guinness Draught",
				"Don't fall for the myth that dark beers are higher in alcohol content and calories. In reality, the roasted barley found in Guinness and other stouts only enriches taste.",
				4.2, "Ireland");
		beer[1] = new Beer("Guinness Blonde American Lager",
				"Irish tradition meets American spirit in the Guinness Blonde American Lager. Light with floral aromas and citrus flavors, this beer is perfectly balanced with a lingering malt and biscuit finish. ",
				5, "Cork");
		beer[2] = new Beer("Harp Lager",
				"Not everyone wants a beer to taste like a milkshake. Luckily for them, there's hope — Harp. This crisp summery lager, which comes from a country better known for its stouts and leprechauns, has a bitter beginning that quickly turns to clean and refreshing. This classic lager is smooth and solid.",
				4.5, "Kilkenny");
		beer[3] = new Beer("Kilkenny Irish Cream Ale",
				"Kilkenny has friends in high places. Guinness brews it; Diageo, the world's largest producer of spirits (Smirnoff, Johnnie Walker, Jose Cuervo, Bailey's and Guinness) carries it; and Smithwick's, Ireland's oldest brewery, is where it originated.",
				4.5, "New Ross");
		beer[4] = new Beer("Murphy's Irish Red",
				"Irish red ales get their reddish hue from the small amounts of roasted barley they contain. Some manufacturers artificially color their beers red, and as a result some beers labeled red ales are not truly so.",
				5, "Dublin");
		beer[5] = new Beer("Murphy's Irish Stout",
				"The lightest and sweetest of Ireland's Big Three (Guinness, Beamish and Murphy's), Murphy's Irish Stout is the nice guy of the group. But don't be deceived — that just means you can drink more of ‘em. Think chocolate milk topped with a double shot of espresso and finished with a one-inch thick head of caramel-infused creamy goodness. ",
				4, "Limerick");
		beer[6] = new Beer("O'Hara's Celtic Stout (Carlow Brewery)",
				"Carlow Brewery is what you would call old school. Its name comes from Carlow, a small town located in Ireland's historic Barrow Valley region and home to a once-thriving craft beer scene. ",
				4.3, "Galway");
		beer[7] = new Beer("O'Hara's Curim gold Irish Wheat (Carlow Brewery)",
				"Ireland is famous for its stout, but this Irish wheat beer is worth checking out. Also known as Curim Gold Celtic Wheat, O'Hara's Irish Wheat is an easy-drinking golden ale. On the nose, it displays hints of banana, peach and plum, which balance the bitter hops on the finish. ",
				4.3, "Tramore");
		beer[8] = new Beer("Porterhouse Brewing Co. Oyster Stout",
				"Established in 1996, Porterhouse is Ireland's largest independent brewery. Beginning with a Dublin pub, the company now operates bars as far afield as New York and London, bringing their craft brews beyond the Emerald Isle's shores. Porterhouse Brewing Company makes a varied range of stouts, ales, lagers, seasonal and specialty beers, including their popular oyster stout. The name is not a misnomer.",
				5.2, "Wexford");
		beer[9] = new Beer("Smithwick's Irish Ale",
				"While Ireland is known in America for its hearty stouts, the country also has a long history of brewing crisp red ales. Smithwick's has been crafting its own rendition since 1710, although the original brewery dates back as far as the 14th century.",
				4.5, "Kilarney");

		Random r = new Random();
		int i = r.nextInt(beer.length);

		return beer[i];

	}

}
