package ie.wit.www;

public class Beer {

	private String name;
	private String desc;
	private double perc;
	private String loc;

	public Beer(String name, String desc, double perc, String loc) {
		this.name = name;
		this.desc = desc;
		this.perc = perc;
		this.loc = loc;

	}

	public String getName() {
		return this.name;
	}

	public String getDesc() {
		return this.desc;
	}

	public double getPerc() {
		return perc;
	}

	public String getLoc() {
		return loc;
	}

}
