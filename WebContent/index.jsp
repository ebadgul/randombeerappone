<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<link rel="stylesheet" type="text/css" href="stylesheets/mycss.css">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Random Beer App</title>
</head>
<jsp:include page="navbar.jsp" />
<body>


	<div id="menu" class>
		<ul>
			<li><h1>The Random Beer App</h1></li>
			<li><form action="MainServlet" method="get">
				<input type="submit" value="Show Another Beer" class="btn btn-info" id="adjst"/>
			</form></li>
			
		</ul>
		
		<h2>Name: ${name}</h2>
		<p>Desc: ${desc}</p>
		<p>ABV: ${ABV}</p>
		<p>Location: ${location}</p>
		
	</div>




</body>
</html>